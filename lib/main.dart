import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/provider/TaskProvider.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:todo_list/view/CreateSubTask.dart';
import 'package:todo_list/view/CreateTask.dart';
import 'package:todo_list/view/Home.dart';
import 'package:todo_list/view/TaskDetail.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => TaskProvider()),

      ],
      child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'What\'s Sup vorn',

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor:Color( Appcolor.maincolor),
        iconTheme: IconThemeData(
          color: Colors.white
        ),

        scaffoldBackgroundColor: Color(Appcolor.maincolor),
        appBarTheme: AppBarTheme(
            backgroundColor: Color(Appcolor.maincolor)
        ),


        useMaterial3: true,
      ),
      initialRoute: '/',
      routes:{
        '/':(context) => MyHomePage(title: "Welcome back",),
        '/detail':(context) => Taskdetail(),
        '/create':(context)=>CreateTask(),
        '/subcreate':(context)=>CreateSubTask(),
      },
      //
      // home: const MyHomePage(),
    );
  }
}


