 class Appcolor{
  static const maincolor = 0xffb1C2D4E;
  static const bgcolor =0xffb1C2D4E;
  static const secondary =0xffb7895CB;
  static const accendsecondary = 0xffbA0BFE0;
  static const lightaccent = 0xffbC5DFF8;

  static const firstpanel =0xffb81B8F2;
  static const secondpanel =0xffb7895CB;

  static const thirdpanel =0xffb4D7BD2;
  static const fourthpanel =0xffbA0B5DA;
  static const lastpanel =0xffb4668AA;

  static const donepanel =0xff4668AA;
}