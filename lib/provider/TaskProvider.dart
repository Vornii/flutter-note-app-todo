// ignore_for_file: public_member_api_docs, lines_longer_than_80_chars
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/helper/database.dart';

import '../model/Task.dart';

class TaskProvider with  ChangeNotifier{

List<Task> _items =  [

  // Task(id: 1,title: "Final Exam This",
  //
  //     subtask: null,date: DateTime.now()
  // ),
  // Task(id: 2,title: "Planning Birthday",
  //
  //     subtask: null,date: DateTime.now()
  // ),
  // Task(id: 3,title: "Lesson Plan",
  //
  //     subtask: null,date: DateTime.now()
  // ),
  // Task(id: 4,title: "Amy Birthday"
  //
  //     ,subtask: null,date: DateTime.now(),isFavorite: true
  // ),
];

List<Task> get getitems => _items;

  set items(List<Task> value) {
    _items = value;
  }

Task getid(id){
   var  newlist =_items.firstWhere((element) => element.id == id);
   notifyListeners();
  return newlist;

  }

  void insertdata() async{

  }
  void fetchdatasub() async{
    print("Fetch data sub ");
    var item = await Dbhelper.getsubmain();
    var all = item.map((e) {
      return e.toString();
    },).toList();
    print(all);
  }

Future<List<Task>> fetchdata() async{
    print("Fetch data mainsub ");
    // var getsubtask

    var items = await Dbhelper.getall();
    print(items);

   var listsub = await Dbhelper.getsubmain();
   List<SubTask> std = [];
  std = listsub.map((e) => SubTask(e["id"],e["title"], e["isdone"],tid: e["taskid"])).toList();
  print(std.toString());

// _items.clear();
    _items = items.map((item) {

     List<SubTask> s = std.where((element) => element.tid == item["taskid"]).toSet().toList();





      return  Task(
        id:item['taskid'] ,
        total: item['total'],
        isFavorite: item['isFavorite'],
        title: item['title'],
        date: item['date'],
        subtask:s,

      );
    }).toSet().toList();
    // print(all);
 return _items;


  }
Future<void>fetchalltasks() async{
  print("Fetch data mainsubtasks ");
  var item = await Dbhelper.getall();
  var all = item.map((e) {
    return e.toString();
  },).toList();
  print(all);
}
void  addFav(){

  }
 List<Task>?  getFavTask(){
    var  allfavorite =_items.where((element) => element.isFavorite ==1).toList();
    return allfavorite;
  }

  void addFavTask(id,index) async{
    //TODO update to favorite or unfavorite
    var task =_items.firstWhere((element) => element.id == id);
    _items.removeWhere((element) => element.id == id);
    if(task.isFavorite ==0){
      task.isFavorite = 1;
    }
    else{
      task.isFavorite = 0;
    }
    // task.isFavorite =! task.isFavorite;
   _items.insert(index,    task);
    await Dbhelper.updatetask(id, {
      'id': id,
      'title':task.title,
      'date':  task.date,
      'isfavorite': task.isFavorite ,
      'total': task.total,
    });

   notifyListeners();

  }


void updateMaintask(id){

}

void updateTask(id,Task? oldtask,newtitle,isFav,newDate) async{

  var task =_items.firstWhere((element) => element.id == id);
var index = _items.indexOf(task);
_items.removeWhere((element) => element.id == id);
//TODO overwrite the old task to new task
  task.title = newtitle;
  task.isFavorite = isFav ?? oldtask?.isFavorite;

  task.subtask = oldtask?.subtask;
  task.date = newDate ?? oldtask?.date;

   _items.insert(index, task);
   await Dbhelper.updatetask(id, {
     'id': id,
     'title':newtitle,
     'date':  task.date.toString(),
     'isfavorite': task.isFavorite ,
     'total': task.total,
   });
  notifyListeners();

}
  void additem(Task task,SubTask subTask) async{
    //TODO Add main Task here
    _items.add(task);

    print("Reach to database");


var tasks = await Dbhelper.insertmaintask('main_task',{

      'title' :task.title,
      'date' : task.date.toString(),
      'isFavorite':task.isFavorite,
      'total':0
    });
int tid =int.parse( tasks[0]["id"].toString());
    Dbhelper.insertsubtask('sub_task', {
      'title' :subTask.title,
      'isdone' : 0,
      'taskid':tid,

    });

    notifyListeners();





  }
  void deleteMainTask(id) async{
    _items.removeWhere((element) => element.id == id);
    print("maintask id $id");
   Dbhelper.deletebyid(id);
    notifyListeners();
  }
void patchsubtask(txtsubtitle,id,sid){
  var task =_items.firstWhere((element) => element.id == id);
  var index = _items.indexOf(task);
  _items.removeWhere((element) => element.id == id);
//TODO overwrite the old task to new task


  var subtasks = task.subtask?.firstWhere((element) => element.id == sid);
  var indexsub = task.subtask?.indexOf(subtasks!);
  print(indexsub);
  print(subtasks);
  subtasks?.title = txtsubtitle;

  task.subtask!.removeAt(indexsub!);

  task.subtask!.insert(indexsub, subtasks!);

  _items.insert(index, task);
  print("updating sub title?");

  Dbhelper.updateallsub(task.subtask![index].id, {
    'title' :subtasks ?.title,
    'isdone' : subtasks ?.isDone,
    'taskid':id,

  });
  notifyListeners();



}

void updatetitlesub(id,SubTask? sub ){
  var task =_items.firstWhere((element) => element.id == id);


  var index = _items.indexOf(task);
  _items.removeWhere((element) => element.id == id);
//TODO overwrite the old task to new task


  task.subtask?.add(sub!);




  _items.insert(index, task);





  notifyListeners();
}
  void updatesubtask(id,SubTask? sub,{isupdate}){
    var task =_items.firstWhere((element) => element.id == id);


    var index = _items.indexOf(task);
    _items.removeWhere((element) => element.id == id);
//TODO overwrite the old task to new task


    task.subtask?.add(sub!);




    _items.insert(index, task);



      Dbhelper.insertsubtask('sub_task', {
        'title' :sub?.title,
        'isdone' : 0,
        'taskid':id,

      });


    notifyListeners();
  }




 void updateTotalTask(id){
   var task =_items.firstWhere((element) => element.id == id);

 }
  void updatestatusSub(id,index ) async{
    var task =_items.firstWhere((element) => element.id == id);
    int? sub ;
    if(  task.total== null){
      task.total = 0;
    }
    if(  task.total <= task.subtask?.length){

      if(task.subtask?[index].isDone == 1){
        // let say we have total = 5
        // t = 5 -  1 =4
        //t = 1
        if(task.total !=0){
          task.total = task.total - 1;
        }
        task.total = task.total;

        //1

      }
      if(task.subtask?[index].isDone == 0){
        if(task.total > task.subtask?.length){
          task.total = task.total;
        }
        task.total = task.total + 1;

      }
      sub = task.total;

    }

  print("Update state");
    if(task.subtask![index].isDone ==0){
      task.subtask![index].isDone =1;
     await Dbhelper.updatesubtaskstatus( task.subtask![index].id, 1,mid: id,isdone: 1,total:sub);
    }
    else{
      task.subtask![index].isDone =0;
     await Dbhelper.updatesubtaskstatus( task.subtask![index].id, 0,mid: id,isdone: 0,total:sub);

    }

    notifyListeners();
  }


}