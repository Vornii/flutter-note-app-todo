

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:todo_list/view/CreateSubTask.dart';
import 'package:todo_list/view/CreateTask.dart';

import '../model/Task.dart';
import '../provider/TaskProvider.dart';

class Taskdetail extends StatefulWidget {
  var id;
 Taskdetail({Key? key,this.id}) : super(key: key);

  @override
  State<Taskdetail> createState() => _TaskdetailState();
}

class _TaskdetailState extends State<Taskdetail> {
  var isDone = false;
  var _currindex;

  var ls =[];

  var comlen = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }
  Widget build(BuildContext context) {

  var ls  = Provider.of<TaskProvider>(context).getid(widget.id) ;
  var list = Provider.of<TaskProvider>(context);


    return Scaffold(
      appBar: AppBar(

        iconTheme: IconThemeData(
          color: Colors.white
        ),
        elevation: 0,
        actions: [
          IconButton(onPressed: null,
              icon: Icon(Icons.add)

          )
        ],


      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
            return Consumer<TaskProvider>(
              builder: (context, value, child) {
                return Column(
                  children: [
                    //TODO TOP Pannel
                    Padding(
                      padding: const EdgeInsets.all(8.0),

                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(Appcolor.thirdpanel),
                            borderRadius: BorderRadius.circular(12)
                        ),
                        width: double.maxFinite,
                        height: constraints.maxHeight*0.20,
                        child:
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [

                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('${ls.title}',style: TextStyle(color:
                                  Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 20
                                  ),),

                                  GestureDetector(
                                    onTap: () {

                                      Navigator.push(context,MaterialPageRoute(builder: (context) {
                                        return CreateTask(isEdit: true,task: ls,);
                                      },));
                                    },
                                    child: CircleAvatar(
                                      radius: 15,
                                      backgroundColor: Color(Appcolor.maincolor),
                                      child: Icon(Icons.edit,color: Colors.white,size: 13,),
                                    ),
                                  )
                                ],
                              ),


                              Card(

                                color: Color(Appcolor.maincolor),
                                elevation: 0,

                                child:  Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text('${ls.total } of ${ls.subtask?.length} completed Tasks',style: TextStyle(color:
                                  Colors.white,


                                      fontSize:12
                                  ),),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),

                    Expanded(

                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Today Task',style:
                                TextStyle(color:
                                Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16
                                ),



                                ),
                                Card(

                                  color: Color(Appcolor.secondary),

                                  elevation: 0,

                                  // child:  Padding(
                                  //   padding: const EdgeInsets.all(7.0),
                                  //   child: Text('Due ${DateFormat.yMMMd().format(ls.date)}',style: TextStyle(color:
                                  //   Colors.white,
                                  //
                                  //       fontSize:12
                                  //   ),),
                                  // ),
                                )
                              ],
                            ),
                            SizedBox(height: 8,),
                            //TODO List of today task

                            Expanded(


                                child: ListView.builder(


                                  itemCount: ls.subtask?.length ?? 0,

                                  itemBuilder: (context, index) {


                                    return Card(
                                      margin: EdgeInsets.only(top: 8),
                                      elevation: 0,
                                      color:
                                      index.isEven == true ?


                                      Color(
                                          ls.subtask![index].isDone ==0?     Appcolor.thirdpanel :

                                          Appcolor.donepanel

                                      ) :
                                      Color(

                                          ls.subtask![index].isDone ==1?        Appcolor.lastpanel:

                                          Appcolor.secondary

                                      ),
                                      child: ListTile(
                                        key: UniqueKey(),
                                        title:


                                        Text("${     ls.subtask![index].title}",

                                          style: TextStyle(
                                            color:
                                            ls.subtask![index].isDone ==0?
                                            Colors.white : Color(Appcolor.maincolor),
                                            decoration:
                                            ls.subtask![index].isDone == 1 ?
                                            TextDecoration.lineThrough : null,

                                          ),),
                                        trailing:  GestureDetector(
                                          onTap: () {
                                            Navigator.push(context,MaterialPageRoute(builder: (context) {
                                              return CreateSubTask(isEdit: true,sub:   ls.subtask![index],task:ls);
                                            },));
                                          },
                                          child: Icon(Icons.edit,
                                            size: 16,

                                            color: Colors.white,),
                                        ),
                                        leading:             GestureDetector(
                                          onTap: () {
                                            setState(() {


                                              _currindex = index;

                                              print(index);
                                              //TODO update list new
                                              list.updatestatusSub(ls.id,_currindex);
                                              if(ls.subtask![index].isDone == 1){
                                                comlen +=1;

                                              }
                                              else{
                                                comlen-=1;
                                              }




                                            });
                                          },
                                          child:

                                          CircleAvatar(
                                            radius: 12,
                                            backgroundColor:

                                            ls.subtask![index].isDone == 0?
                                            Color(Appcolor.fourthpanel):
                                            Color(Appcolor.maincolor)

                                            ,
                                          ),
                                        ),
                                      ),
                                    );
                                  },)


                            ),


                          ],
                        ),
                      ),
                    )
                  ],
                );
              },

            );
        },

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.push(context,

              MaterialPageRoute

                (
                fullscreenDialog: true,
                builder: (context) {
                  return CreateSubTask(task:ls,detail:true);
                },));
        },
        hoverColor: Color(Appcolor.lastpanel),
        autofocus: true  ,

        backgroundColor: Color(Appcolor.fourthpanel),
        child: Icon(
            Icons.edit
        ),
      ),
    );
  }
}
