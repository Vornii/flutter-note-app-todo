

import 'dart:math';
import 'package:bulleted_list/bulleted_list.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/provider/TaskProvider.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:todo_list/view/TaskDetail.dart';

import '../model/Task.dart';

class AllScreen extends StatefulWidget {

AllScreen({Key? key}) : super(key: key);

  @override
  State<AllScreen> createState() => _AllScreenState();
}

class _AllScreenState extends State<AllScreen> {



 var listcolor = [
 Appcolor.secondpanel,
   Appcolor.thirdpanel
 ];
 var _currentItem;

  var isExpanded = false ;
  @override

  void initState() {
    // TODO: implement initState
    super.initState();




  }
 futurebuild() async  {

 }
  Widget build(BuildContext context) {
    // print(isExpanded);
      List<Task>? ls = Provider.of<TaskProvider>(context).getitems.toSet().toList();
    var listoftask =Provider.of<TaskProvider>(context);
      var lss = Provider.of<TaskProvider>(context);
      // lss.insertdata();



      //
      //
      // lss.fetchdata();
      // lss.fetchdatasub();
      // lss.fetchalltasks();

    return FutureBuilder(
        future: Provider.of<TaskProvider>(context,listen: false).fetchdata(),
      builder: (context, snapshot) {
        ls = snapshot?.data;


        if(snapshot.connectionState != ConnectionState.done){
            return Center(
              child:CircularProgressIndicator(

              ) ,
            );
          }
          else{

            return Consumer<TaskProvider>(
              builder: (context, value, _) {
                print(ls!.length);
                return Padding(

                  padding: const EdgeInsets.all(8.0),
                  child: Container(

                      child:
                      ls!.length == 0 ?
                      Center(child: Text("You have no task yet",style: TextStyle(
                          color: Color(Appcolor.lastpanel)
                      ),),)    :


                      ListView.builder(
                        itemCount : ls!.length ?? 0,
                        itemBuilder: (context, index) {

                          return Dismissible(

                            confirmDismiss: (direction) {
                              return showDialog(context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    elevation: 0,
                                    alignment: Alignment.centerLeft,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10)
                                    ),
                                    title: Row(
                                      children: [
                                        Expanded(child: Row(children: [
                                          Icon(Icons.android_sharp,
                                            size: 25,
                                            color: Color(
                                              Appcolor.maincolor,


                                            ),),
                                          SizedBox(width: 5,),
                                          Text("Warning",style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                              color: Color(Appcolor.maincolor)
                                          ),),
                                        ],)),

                                        InkWell(
                                          onTap: () => Navigator.pop(context),
                                          child: Icon(Icons.close,
                                            size: 25,
                                            color: Color(
                                              Appcolor.maincolor,


                                            ),),
                                        ),
                                      ],
                                    ),

                                    content: Container(
                                      child: Text(

                                        "You're about to complete this main task. Once comfirm you cant return back",style: TextStyle(
                                        fontSize: 13,

                                      ),),
                                    ),


                                    actions: [


                                      Container(
                                        width: double.maxFinite,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: Color(Appcolor.lastpanel),//<-- SEE HERE
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(12.0),

                                            ),
                                          ),

                                          child: Text("Comfirm",style: TextStyle(color: Colors.white),),
                                          onPressed: () {
                                            //TODO Delete or remove the maint task
                                            listoftask.deleteMainTask(ls![index].id);
                                            Navigator.pop(context);
                                            ScaffoldMessenger.of(context).showSnackBar(



                                                SnackBar(

                                                  content:Text("Your Task has been completed") ,
                                                  backgroundColor: Color(Appcolor.lastpanel),

                                                ));
                                          },
                                        ),
                                      ),



                                    ],

                                  );
                                },

                              );
                            },

                            key: UniqueKey(),
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Card(

                                color:


                                index.isEven == true ?
                                ls![index].total ==ls![index].subtask?.length ?
                                Color(0xff4668AA) :
                                Color(Appcolor.secondpanel) :
                                ls![index].total ==ls![index].subtask?.length ?
                                Color(0xff4668AA) :

                                Color(Appcolor.fourthpanel),


                                child: ListTile(

                                  title: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('${ls![index].title}',style: TextStyle(
                                              color: Color(Appcolor.maincolor),
                                              // color:Color(Appcolor.accendsecondary),
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600
                                          ),),

                                          GestureDetector(
                                              onTap: (){
                                                setState(() {
                                                  print(isExpanded);
                                                  isExpanded =! isExpanded;
                                                  _currentItem = index;
                                                });

                                              },
                                              child: Icon(
                                                isExpanded == true && index == _currentItem?
                                                Icons.expand_less:Icons.close_fullscreen_sharp,
                                                color: Color(Appcolor.maincolor),
                                                size: isExpanded == true && index == _currentItem? 25 : 20,


                                              )),

                                        ],
                                      ),

                                      Divider(       color: Color(Appcolor.maincolor),),


                                    ],

                                  ),



                                  subtitle:


                                  isExpanded && index == _currentItem?
                                  //TODO Expanded here
                                  Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top: 20),
                                        child: ListTile(

                                          title: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(child: Row(children: [
                                                Icon(Icons.featured_play_list,color: Color(Appcolor.maincolor),),
                                                SizedBox(width: 10,),
                                                Text("Things to do",style: TextStyle(
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500,
                                                ),),

                                              ],)),

                                              InkWell(
                                                onTap: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                    return Taskdetail(id: ls![_currentItem].id,);
                                                  },));
                                                },
                                                child: Icon(Icons.arrow_circle_right
                                                  ,

                                                  color: Color(
                                                      Appcolor.maincolor
                                                  ),

                                                  size: 25,),
                                              )




                                              // Container(
                                              //   width:80,
                                              //   height: 25,
                                              //   child: ElevatedButton(
                                              //     style: ElevatedButton.styleFrom(
                                              //       shape: RoundedRectangleBorder(
                                              //         borderRadius: BorderRadius.circular(5),
                                              //
                                              //       ),
                                              //       backgroundColor: Color(Appcolor.maincolor)
                                              //     ),
                                              //     onPressed: () {
                                              //       Navigator.push(context, MaterialPageRoute(builder: (context) {
                                              //         return Taskdetail(id: ls[_currentItem].id,);
                                              //       },));
                                              //     },
                                              //     child:
                                              //
                                              //     Text("See",style: TextStyle(
                                              //       color: Color(Appcolor.fourthpanel),
                                              //       fontSize: 13
                                              //     ),
                                              //
                                              //     ),
                                              //   ),
                                              // )
                                            ],
                                          ),
                                          subtitle:  Column(
                                            children: [


                                              Padding(

                                                padding: const EdgeInsets.only(top: 10),
                                                child: BulletedList(
                                                    listOrder: ListOrder.ordered,

                                                    // bulletType: BulletType.numbered,

                                                    style: TextStyle(
                                                        color: Color(Appcolor.lastpanel)
                                                    ),
                                                    bulletColor: Color(Appcolor.maincolor),

                                                    listItems: List.generate(ls![_currentItem].subtask?.length ?? 0, (index) {

                                                      return ListTile(

                                                        title: Text("${ls![_currentItem].subtask![index].title}",style:

                                                        TextStyle(
                                                            fontSize: 13,
                                                            color: Color(Appcolor.maincolor),
                                                            fontWeight: FontWeight.w400,
                                                            height: 1

                                                        ),),
                                                      );
                                                    })

                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),

                                    ],
                                  )

                                      :  Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Consumer<TaskProvider>(
                                        builder: (context, value, child) {
                                          return Expanded(child: Row(children: [
                                            //TODO Favorite Icon here
                                            GestureDetector(

                                              child: Icon(
                                                ls![index].isFavorite == 1 ? Icons.favorite :
                                                Icons.favorite_border_rounded
                                                ,
                                                size: 20,

                                                color:

                                                Color(Appcolor.maincolor),),
                                              onTap: () {
                                                listoftask.addFavTask(ls![index].id, index);
                                                //TODO update to favorite

                                                if(  ls![index].isFavorite == 1){
                                                  ScaffoldMessenger.of(context).hideCurrentSnackBar(
                                                      reason: SnackBarClosedReason.dismiss
                                                  );

                                                  ScaffoldMessenger.of(context).showSnackBar(

                                                      SnackBar(

                                                        content: Text('Task has added to favorite'),


                                                        backgroundColor: Color(Appcolor.maincolor),

                                                      ));
                                                }
                                                else{
                                                  ScaffoldMessenger.of(context).hideCurrentSnackBar(
                                                      reason: SnackBarClosedReason.dismiss
                                                  );
                                                  ScaffoldMessenger.of(context).showSnackBar(

                                                      SnackBar(content: Text('Task has removed from favorite'),
                                                        backgroundColor: Color(Appcolor.maincolor),

                                                      ));
                                                }

                                              },
                                            ),
                                            SizedBox(width: 10,),



                                              // Text('You have ${ls[index].subtask?.length ?? 0} tasks',style: TextStyle(
                                              //     color: Color(Appcolor.maincolor)
                                              // )),
                                            Text(ls![index].total ==ls![index].subtask?.length ? 'Tasks Completed' : 'You have ${ls![index].subtask?.length ?? 0} tasks',style: TextStyle(
                                                color: Color(Appcolor.maincolor)
                                            )),
                                          ],));
                                        },

                                      ),

                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [

                                          IconButton(onPressed:null,
                                            icon: Icon(Icons.calendar_today_rounded,
                                              color: Color(Appcolor.maincolor),

                                              size: 18,),


                                          ),
                                          Text('${ DateFormat.yMMMMd('en_US').format(DateTime.parse(ls![index].date.toString()) )}',style: TextStyle(
                                              fontSize: 13,
                                              color: Color(Appcolor.maincolor)

                                          )),
                                        ],
                                      )

                                    ],

                                  ),

                                ),
                              ),
                            ),
                          );
                        },)
                  ),
                );
              },

            );
          }

      },

    );
  }

}
