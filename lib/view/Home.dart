import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:todo_list/view/CreateTask.dart';
import 'package:todo_list/view/FavScreen.dart';
import 'package:todo_list/view/widget/DrawerBar.dart';

import 'AllScreen.dart';

class MyHomePage extends StatefulWidget {

  String? title;
  var indicator;
  MyHomePage({Key? key,this.title,this.indicator}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      initialIndex:
      widget.indicator ?? 0
      ,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,


          iconTheme: IconThemeData(color: Colors.white),
              title:  Text("${widget!.title}",


              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 25,

              ),),

          bottom: TabBar(


            dividerColor:Colors.transparent,
            isScrollable: true,



            labelColor: Color(Appcolor.secondary),
            unselectedLabelColor:Color(Appcolor.fourthpanel),

            indicatorColor: Color(Appcolor.accendsecondary),
            tabs: [

              Tab(
                child: Text('All',style: TextStyle(

                ),),
              ),
              Tab(
                child: Text('Favorite',style: TextStyle(
     
                ),),
              ),
            ],
          ),
          ),
        endDrawer: DrawerBar(),




        body: SafeArea(

          child: TabBarView(
            children: [


              AllScreen(),
              FavScreen()
            ],
          )

        ),
       floatingActionButton: FloatingActionButton(
         onPressed: (){
           Navigator.push(context,

               MaterialPageRoute

                 (
                 fullscreenDialog: true,
                 builder: (context) {
             return CreateTask();
           },));
         },
         hoverColor: Color(Appcolor.lastpanel),
         autofocus: true  ,

         backgroundColor: Color(Appcolor.fourthpanel),
         child: Icon(
           Icons.add
         ),
       ),
      ),
    );
  }
}


