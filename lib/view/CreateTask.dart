

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/provider/TaskProvider.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:todo_list/view/CreateSubTask.dart';

import '../model/Task.dart';

class CreateTask extends StatefulWidget {
  Task? task;
  var isEdit;

  CreateTask ({Key? key,this.task,this.isEdit}) : super(key: key);

  @override
  State<CreateTask> createState() => _CreateTaskState();
}

class _CreateTaskState extends State<CreateTask> {
  var txttask = TextEditingController();
  var isFavorite = false;
  String? picker;
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.isEdit != null){
      txttask.text = widget.task!.title;
      isFavorite = true;
      picker = widget.task!.date;
    }
  }
  Widget build(BuildContext context) {
    var task = Provider.of<TaskProvider>(listen: false,context);

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: Colors.white
        ),

      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.isEdit == true ?
                          'Edit' : 'Create New',style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.w500
                        ),),

                        Container(
                          margin: EdgeInsets.only(
                              top: 20
                          ),
                          child: TextField(
                            controller: txttask,
                            onSubmitted: (value) {
                              setState(() {
                                txttask.text = value.toString();
                              });
                            },
                            style: TextStyle(
                              color:Colors.white,
                            ),
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:Color(Appcolor.fourthpanel)
                                  )
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color:Color(Appcolor.fourthpanel)
                                  )
                              ),
                              border: UnderlineInputBorder(

                                  borderSide:
                                  BorderSide(
                                      color: Color(Appcolor.fourthpanel)


                                  )
                              ),


                              label: Text('Task Name',style:

                              TextStyle(
                                  color: Color(Appcolor.fourthpanel,


                                  ),
                                  fontWeight: FontWeight.w400
                              ),),

                            ),
                          ),
                        ),
                        SizedBox(height: 40,),
                        //TODO Icon button
                        Row(
                          children: [
                            ElevatedButton.icon(

                                style: ButtonStyle(

                                  backgroundColor: MaterialStatePropertyAll(Color(Appcolor.thirdpanel)),

                                ),


                                label:


                                Text(        picker != null ? DateFormat.yMMMMd('en_US').format(DateTime.parse(picker.toString()) ): "Add Date" ,style: TextStyle(
                                    color: Color(Appcolor.maincolor)
                                ),),
                                icon: Icon(Icons.add,    color: Color(Appcolor.maincolor)),
                                onPressed: () async{
                                  showDatePicker(context: context,
                                      builder: (context, child) {
                                        return Theme(
                                          data: Theme.of(context).copyWith(
                                            colorScheme: ColorScheme.light(
                                              primary: Color(Appcolor.lastpanel), // <-- SEE HERE
                                              onPrimary:Colors.white, // <-- SEE HERE

                                            ),
                                            // textButtonTheme: TextButtonThemeData(
                                            //   style: TextButton.styleFrom(
                                            //     primary: , // button text color
                                            //   ),
                                            // ),
                                          ),
                                          child: child!,
                                        );
                                     
                                      },
                                      confirmText:'Choose' ,
                                      initialDate: DateTime.now(),
                                      firstDate:DateTime(1900),
                                      lastDate: DateTime(2025),


                                  ).then((value) {
                                   setState(() {


                                     picker= value.toString();
                                     print(picker);
                                     ScaffoldMessenger.of(context).showSnackBar(

                                         SnackBar(content: Text('Date has been choosen'),
                                           backgroundColor: Color(Appcolor.maincolor),

                                         ));
                                   });
                                  });
                                }

                            ),
                            SizedBox(width: 20,),
                            ElevatedButton.icon(
                                style: ButtonStyle(

                                  backgroundColor: MaterialStatePropertyAll(Color(
                                      isFavorite == true ?   Appcolor.lastpanel :  Appcolor.secondary
                                  )),

                                ),


                                label: Text("Add Favorite",style: TextStyle(
                                    color:

                                    Color(Appcolor.maincolor)
                                ),),
                                icon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isFavorite =!isFavorite;
                                      print(isFavorite);
                                    });
                                  },
                                  child: Icon(

                                    isFavorite == true ?
                                        Icons.check_circle :
                                    Icons.circle,
                                    color: Color(Appcolor.maincolor),
                                  ),
                                ),
                                onPressed: null

                            ),
                          ],
                        ),

                        //TODO Create Button
                      ],
                    )

                ),
        //TODO On Create Task
                Container(
                  width: double.maxFinite,
                  child:
                      widget.isEdit == true ?

                  ElevatedButton


                    (onPressed: () async{
                      print(isFavorite);


              //TODO update main task

             task.updateTask(widget.task!.id,widget.task,txttask.text,isFavorite,picker);
             Navigator.pop(context);

                  },
                      style: ButtonStyle(
                        backgroundColor: MaterialStatePropertyAll(
                          Color(Appcolor.thirdpanel)
                        )
                      ),

                      child: Text(
                        widget.isEdit == true ?
                            "Save" :
                        "Create",style: TextStyle(
                        color: Colors.white,
                          fontSize: 15
                      ),)

                  ):
                  ElevatedButton


                    (onPressed: () async{


                    Navigator.push(

                        context, MaterialPageRoute(
                      fullscreenDialog: true,
                      builder: (context) {
                        return CreateSubTask(txtmain: txttask.text,
                            isEdit: false,
                            detail: false,



                            task: Task(id:DateTime.now(),title:txttask.text,
                                isFavorite:isFavorite == true ? 1 : 0 ,
                              date: picker,


                              isExpand: false,));
                      },));


                  },
                      style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(
                              Color(Appcolor.thirdpanel)
                          )
                      ),

                      child: Text(
                        widget.isEdit == true ?
                        "Save" :
                        "Create",style: TextStyle(
                          color: Colors.black,
                          fontSize: 15
                      ),)

                  ),
                ),
                SizedBox(height: 20,),
              ],
            ),
          );
        },

      ),
    );
  }
}
