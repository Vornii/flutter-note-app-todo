import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../res/constrant/color.dart';

class AppBarView extends StatelessWidget {
  const AppBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(


      iconTheme: IconThemeData(color: Colors.white),
      title: Text("Whats Sup Vorn?",


        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w500,
          fontSize: 28,

        ),),

      bottom: TabBar(

        dividerColor:Colors.transparent,
        isScrollable: true,





        labelColor: Color(Appcolor.secondary),
        unselectedLabelColor: Colors.white.withOpacity(0.86),

        indicatorColor: Color(Appcolor.accendsecondary),
        tabs: [
          Tab(
            child: Text('All',style: TextStyle(

            ),),
          ),
          Tab(
            child: Text('Favorite',style: TextStyle(

            ),),
          ),
        ],
      ),
    );
  }
}
