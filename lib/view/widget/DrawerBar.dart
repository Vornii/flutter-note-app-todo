import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/res/constrant/color.dart';
import 'package:url_launcher/url_launcher.dart';

import '../CreateTask.dart';
import '../Home.dart';

class DrawerBar extends StatelessWidget {

  const DrawerBar ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Drawer(
      backgroundColor: Color(Appcolor.maincolor),

    child: Column(
    children: [

      AppBar(
        automaticallyImplyLeading: false,
        iconTheme: IconThemeData(
          color: Color(Appcolor.lastpanel),
        ),
        title: Row(
          children: [
            Icon(Icons.android_sharp,color:
              Color(Appcolor.secondary),size: 35,),
            SizedBox(width: 20,),
            Text("Note Do",style:
              TextStyle(
                color:
                Color(Appcolor.accendsecondary),
                fontWeight: FontWeight.bold
              )
              ,),
          ],
        ),


        backgroundColor: Color(Appcolor.lastpanel),
      ),
      SizedBox(height: 20,),
      Expanded(child:
      Column(
       crossAxisAlignment: CrossAxisAlignment.start,


        children: [
          Expanded(child: Column(
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);

                },
                child: ListTile(
                  leading: InkWell(
                    onTap: () {

                    },
                    child: Icon(Icons.list,color:

                    Color(Appcolor.fourthpanel),),

                  ),
                  title: Text("All Lists",style:

                  TextStyle(
                    color:

                    Color(Appcolor.fourthpanel),
                  ),),
                ),
              ),

              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return MyHomePage(title: "Welcome back",indicator: 1,);
                  },));
                },
                child: ListTile(
                  leading: InkWell(
                    child: Icon(Icons.favorite,color:

                    Color(Appcolor.fourthpanel),),

                  ),
                  title: Text("Favorites",style:

                  TextStyle(
                    color:

                    Color(Appcolor.fourthpanel),
                  ),),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Divider(
                  color: Color(Appcolor.fourthpanel),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,

                      MaterialPageRoute

                        (
                        fullscreenDialog: true,
                        builder: (context) {
                          return CreateTask();
                        },));
                },
                child: ListTile(
                  leading: InkWell(
                    child: Icon(Icons.add,color:

                    Color(Appcolor.fourthpanel),),

                  ),
                  title: Text("Create New Task",style:

                  TextStyle(
                    color:

                    Color(Appcolor.fourthpanel),
                  ),),
                ),
              ),
            ],
          ) ),

          ListTile(
            onTap: () => openemail,
            leading: Icon(Icons.attach_email,color:

            Color(Appcolor.lastpanel),),
            title: Text("Developer : Panhavorn",style:

            TextStyle(
              color:

              Color(Appcolor.lastpanel),
              fontSize: 15
            ),),
          ),
        ],
      ))
      
 

],
),
      
);
  }
  Future<void> openemail() async {
    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'Nightpp19@gmail.com',
      query: encodeQueryParameters(<String, String>{
        'subject': 'Report Bug',
      }),
    );

   try{
     await launchUrl(emailLaunchUri);
   }catch(e){
     print(e.toString());
   }
  }

  encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((MapEntry<String, String> e) =>
    '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');

  }

}
