

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/res/constrant/color.dart';

import '../model/Task.dart';
import '../provider/TaskProvider.dart';
import 'Home.dart';

class CreateSubTask extends StatefulWidget {
  var txtmain;

  Task? task;
  SubTask? sub;

  var detail ;
  var isEdit;

  CreateSubTask ({Key? key,this.txtmain,this.task,this.detail,this.isEdit,this.sub}) : super(key: key);

  @override
  State<CreateSubTask> createState() => _CreateSubTaskState();
}

class _CreateSubTaskState extends State<CreateSubTask> {
  var txtsub = TextEditingController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.isEdit == true){
      txtsub.text = widget.sub?.title;
      print(widget.isEdit);
    }
    print("Edit sub task");
    print(widget.detail);
  }
  Widget build(BuildContext context) {
    var task = Provider.of<TaskProvider>(listen: true,context);


    return Scaffold(
      appBar:  AppBar(
        iconTheme: IconThemeData(
            color: Colors.white
        ),

      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text( widget.isEdit == true ? 'Edit Sub Task' : 'Sub Task',style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w500
                    ),),

                    Container(
                      margin: EdgeInsets.only(
                          top: 20
                      ),
                      child: TextField(
                        style: TextStyle(
                          color: Colors.white
                        ),
                        controller: txtsub,
                        onSubmitted: (value) {
                          setState(() {
                            txtsub.text = value.toString();
                          });
                        },
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color:Color(Appcolor.fourthpanel)
                              )
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color:Color(Appcolor.fourthpanel)
                              )
                          ),
                          border: UnderlineInputBorder(

                              borderSide:
                              BorderSide(
                                  color: Color(Appcolor.fourthpanel)


                              )
                          ),


                          label: Text('What is your sub task?',style:

                          TextStyle(
                              color: Color(Appcolor.fourthpanel,


                              ),
                              fontWeight: FontWeight.w400
                          ),),

                        ),
                      ),
                    ),
                    SizedBox(height: 40,),
                    //TODO Icon button


                    //TODO Create Button
                  ],
                ),
                //TODO On Create Task
                // if(widget.isEdit == true)
                //   ElevatedButton(onPressed: (){
                //     print(txtsub.text);
                //     print(widget.task!.id);
                //     // print(widget.sub?.id);
                //     task.patchsubtask(txtsub.text, widget.task?.id, widget.sub?.id);
                //     Navigator.pop(context);
                //   }, child:Text("Edit")),
                Container(
                  width: double.maxFinite,
                  child:


                  ElevatedButton


                    (
                      onPressed: ()=>
                      widget.isEdit == true ?
                          onUpdateSub(context, task) :


                      onAddNewSub(context,task),
                      style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(
                              Color(Appcolor.thirdpanel)
                          )
                      ),

                      child: Text(

                       widget.isEdit == true ? "Save" : "Create Task",style: TextStyle(

                          color: Color(Appcolor.maincolor),
                        fontSize: 16

                      ),)

                  ),
                ),
                SizedBox(height: 20,),
              ],
            ),
          );
        },

      ),
    );
  }

  onAddNewSub(BuildContext context,TaskProvider task) {
    print(txtsub.text);

    SubTask ss = new SubTask(DateTime.now(),txtsub.text, false  );
    List<SubTask> sub = [ss];
    SubTask s = SubTask(DateTime.now(),txtsub.text,0 );

 print("widget detail ${widget.detail}");
    if(widget.detail  == true){
      SubTask s = SubTask(DateTime.now(),txtsub.text,0 );
      task.updatesubtask(widget!.task!.id,s);

      Navigator.pop(context);
      // Navigator.pushReplacementNamed(context, '/');
      return null;
    }


    task.additem(
        Task(subtask:sub,id: widget!.task!.id,

        isExpand: false,


        date: DateTime.now().toString(),
        isFavorite: widget!.task!.isFavorite,
        title: widget!.task!.title
    ),ss);

    Navigator.pop(context);
    Navigator.pushReplacementNamed(context, '/');

  }

  onUpdateSub(BuildContext context, TaskProvider task) {
    // SubTask s = SubTask(DateTime.now(),txtsub.text, false  );
    // task.updatesubtask(widget!.task!.id,s);


      print(txtsub.text);
      print(widget.task!.id);
      // print(widget.sub?.id);
      task.patchsubtask(txtsub.text, widget.task?.id, widget.sub?.id);
      Navigator.pop(context);


      // Navigator.pushReplacementNamed(context, '/');





  }
}
