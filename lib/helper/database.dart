import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqlite_api.dart';

class Dbhelper  {

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }
  static Future<sql.Database> databasemaintask () async{
    final dbpath = await sql.getDatabasesPath();
    final sqldb = await sql.openDatabase(path.join(dbpath,'ss1.db'),
      onConfigure: _onConfigure,


      version:1,onCreate: (db, version) => createdata(db, version),


    );
    return sqldb ;
  }

static Future<List<Map<String, Object?>>> insertmaintask(String? table, Map<String, Object> data) async{
  //
  // final db1 = await Dbhelper.datasubtask('tasks');
  // final db2 = await Dbhelper.datasubtask('subtasks');
  // final db2 = await Dbhelper.datasubtask('subtasks');
  // final db1 = await Dbhelper.datamaintask(table);
  // db1.insert("sub_task", data);

 final db  = await Dbhelper.databasemaintask();



await db.insert("main_task", data,conflictAlgorithm: ConflictAlgorithm.replace);
var id = await db.rawQuery("Select id from main_task order by id desc limit 1");

 return id;
}

  static Future<void> insertsubtask(String? table, Map<String, Object> data) async{
    //
    // final db1 = await Dbhelper.datasubtask('tasks');
    // final db2 = await Dbhelper.datasubtask('subtasks');
    // final db2 = await Dbhelper.datasubtask('subtasks');
    // final db1 = await Dbhelper.datamaintask(table);
    // db1.insert("sub_task", data);

    final db  = await Dbhelper.databasemaintask();



    await db.insert("sub_task", data,conflictAlgorithm: ConflictAlgorithm.replace);



  }



  static Future<void> getsubtask(table) async {
    final db  = await Dbhelper.databasemaintask();



  }

  static Future<List<Map<String, Object?>>> getsub(table) async {
    final db  = await Dbhelper.databasemaintask();
    return db.query("sub_task");

  }
static Future<List<Map<String, Object?>>> getmain() async {

  final db2 = await Dbhelper.databasemaintask();
 return db2.query("main_task");

}


  static Future<List<Map<String, Object?>>> getsubtaskone() async {

    final db2 = await Dbhelper.databasemaintask();
    return db2.rawQuery("Select * from sub_task");

  }





  static Future<List<Map<String, Object?>>> getall() async {

    final db2 = await Dbhelper.databasemaintask();

    return db2.rawQuery("Select DISTINCT  s.*,s.id as sid,m.* from main_task m inner join sub_task s on s.taskid = m.id group by m.id ");

  }
  static Future<void> updatesubtaskstatus(id,state,{mid,isdone,int? total}) async {

    final db2 = await Dbhelper.databasemaintask();
    print("Updating ");
    print("Subid");
    print(id);
    print("Stateid");
    print(state);

    int count =  await db2.rawUpdate('update sub_task SET isdone = ?  where id = ? ',[
      state,id
    ]);
    if(isdone == 1){
      int count2 =  await db2.rawUpdate('update main_task SET total = ? where id = ? ',[
      total,  mid
      ]);
    }
    else{
      int count2 =  await db2.rawUpdate('update main_task SET total = ? where id = ? ',[
      total,  mid
      ]);
    }

    print(count);
  db2.close();

  }


  static Future<void > updatetask(id,Map<String, Object> data) async {
    final db2 = await Dbhelper.databasemaintask();
    // title TEXT, date TEXT , isFavorite integer , total integer
    await db2.rawUpdate("Update main_task set title = ? , date = ? , isFavorite = ?, total = ?  where id = ?",[
      data["title"],
      data["date"],
      data["isfavorite"],
      data["total"],
      data["id"]

    ]);
    db2.close();

  }
  static Future<void> updateallsub(idd,Map<String, Object> data) async {

    final db2 = await Dbhelper.databasemaintask();
    print("Updating sub ");
    print(idd);
    print(data["title"]);
    print(data["isdone"]);
    print(data["taskid"]);
    int count =  await db2.rawUpdate('update sub_task SET title = ?, isdone = ?, taskid = ? where id = ? ',[
  data["title"],  data["isdone"],   data["taskid"], idd
    ]);



    db2.close();

  }



  static Future<void> addsubtask(Map<String, Object> data) async {

    final db2 = await Dbhelper.databasemaintask();
    db2.insert("sub_task", data);
    db2.close();
  }


  static Future<List<Map<String, Object?>>> getsubmain() async {

    final db2 = await Dbhelper.databasemaintask();
    return db2.query("sub_task");

  }
  static void deletebyid(id) async {

    final db2 = await Dbhelper.databasemaintask();
    await db2.rawDelete("Delete from main_task where id = ${id}");

  }
  static Future<Database> createdata(Database db, version) async{

    // return db.execute('Create table sub_task (id integer PRIMARY KEY autoincrement, title TEXT, date TEXT , taskid integer , FOREIGN KEY (taskid) REFERENCES main_task (id) ON DELETE CASCASDE )');

    await db.execute('Create table main_task (id integer PRIMARY KEY autoincrement, title TEXT, date TEXT , isFavorite integer , total integer);');
    await db.execute('Create table sub_task (id integer PRIMARY KEY autoincrement, title TEXT , isdone integer, taskid integer , FOREIGN KEY (taskid) REFERENCES main_task (id) ON DELETE CASCADE)');


    return db;

  }
  //return a current path


}