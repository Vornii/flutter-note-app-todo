
class SubTask{
  var id;
  var title;
var isDone;
  var tid;

  SubTask(this.id, this.title,this.isDone,{this.tid});
}


class Task{
  var id;
  var title;
  var isDone;
  var date;
  var isExpand;
  var isFavorite;
  var total ;
  List<SubTask>?subtask;

  Task({this.id, this.title, this.date, this.subtask,this.isExpand,this.isFavorite,this.total ,this.isDone});
}